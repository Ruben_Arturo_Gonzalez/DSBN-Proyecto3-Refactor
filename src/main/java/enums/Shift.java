package enums;

public enum Shift {
    MORNING("Morning"),
    NOON("Noon"),
    NIGHT("Night");

    private String shift;

    Shift(String shift){
        this.shift = shift;
    }

    public String getShift(){
        return shift;
    }
}
